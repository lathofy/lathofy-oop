<?php 
require_once "animal.php";
require_once "frog.php";
require_once "ape.php";

$kambing = new Animal("shauun");

echo "Nama kambing: " . $kambing->name . "<br>";
echo "Jumlah kaki: ". $kambing->legs . "<br>";
echo "Cold blooded: " . $kambing->cold_blooded . "<br>";
echo "<br><br>";

$apek = new Ape("Apek");

echo "Nama Ape: " . $apek->name . "<br>";
echo "Jumlah kaki: ". $apek->legs . "<br>";
echo "Cold blooded: " . $apek->cold_blooded . "<br>";
echo "Yell: ";
$apek->yell();
echo "<br><br>";

$katak = new Frog("Froggy");

echo "Nama Katak: " . $katak->name . "<br>";
echo "Jumlah kaki: ". $katak->legs . "<br>";
echo "Cold blooded: " . $katak->cold_blooded . "<br>";
echo "Jump: ";
$katak->jump();
?>